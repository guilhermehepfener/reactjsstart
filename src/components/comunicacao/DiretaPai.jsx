import React from 'react'
import DiretaFilho from './DiretaFilho'

export default props => {
    return (
        <div>
            <DiretaFilho nome="Júnior" idade={20} nerd={true}/>
            <DiretaFilho nome="Augusto" idade={17} nerd={false}/>
        </div>
    )
}