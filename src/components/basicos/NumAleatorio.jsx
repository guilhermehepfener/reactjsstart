import React from 'react';

export default props => {

    const { min, max } = props;

    const aleatorio = parseInt(Math.random() * (max - min)) + min;

    return (
        <div>
            <h2>Valor aleatório</h2>
            <p><strong>Valor min: {min} </strong></p>
            <p><strong>Valor max: {max} </strong></p>
            <p><strong>Valor escolhido {aleatorio} </strong></p>
        </div>
    )
}
