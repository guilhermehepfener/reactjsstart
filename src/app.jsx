import React from 'react';
import Primeiro from './components/basicos/Primeiro';
import ComParametro from './components/basicos/ComParametro';
import Fragmento from './components/basicos/Fragmento';
import NumAleatorio from './components/basicos/NumAleatorio';
import Card from './components/layout/Card';
import Familia from './components/basicos/Familia';
import FamiliaMembro from './components/basicos/FamiliaMembro';
import ListaAlunos from './components/repeticao/ListaAlunos';
import ListaProdutos from './components/repeticao/ListaProdutos';
import ParOuImpar from './components/condicional/ParOuImpar';
import UsuarioInfo from './components/condicional/UsuarioInfo';
import DiretaPai from './components/comunicacao/DiretaPai';
import IndiretaPai from "./components/comunicacao/IndiretaPai";
import Input from './components/formulario/Input';
import Contador from './components/contador/Contador';

import { Fragment } from 'react';

import './app.css';

export default () =>
    <div className="app">
        <h1>Fundamentos react </h1>
        <div className="Cards">

            <Card titulo="#12 - Contador" color="#555555">
                <Contador numeroInicial={10}></Contador>
            </Card>

            <Card titulo="#11 - Componente Controlado" color="#654321">
                <Input></Input>
            </Card>

            <Card titulo="#10 - Comunicação Indireta" color="#98223c">
                <IndiretaPai></IndiretaPai>
            </Card>

            <Card titulo="#09 - Comunicação Direta" color="#a1b2c3">
                <DiretaPai></DiretaPai>
            </Card>

            <Card titulo="#08 - Renderização Condicional" color="#abc123">
                <ParOuImpar numero={20}></ParOuImpar>
                <UsuarioInfo usuario={{ nome: 'Fernando' }} />
                <UsuarioInfo />
            </Card>

            <Card titulo="#07 - Repetição challenge" color="#123abc">
                <ListaProdutos></ListaProdutos>
            </Card>

            <Card titulo="#06 - Repetição" color="#00f829">
                <ListaAlunos></ListaAlunos>
            </Card>

            <Card titulo="#05 - Componente com filhos" color="#00c8f8">
                <Familia sobrenome="Hepfener">
                    <FamiliaMembro nome="Guilherme" />
                    <FamiliaMembro nome="Marcia" />
                    <FamiliaMembro nome="Carlos" />
                </Familia>
            </Card>
            <Card titulo="#04 - Desafio aleatório" color="#fa6900">
                <NumAleatorio
                    min={1}
                    max={60}
                />
            </Card>

            <Card titulo="#03 - Fragmento" color="#e94c6f">
                <Fragmento />
            </Card>

            <Card titulo="#02 - Com parâmetro" color="#e8b71a">
                <ComParametro
                    titulo="Segundo Componente"
                    subtitulo="Muito Legal" />
            </Card>

            <Card titulo="#01 - Primeiro componente" color="#588c73">
                <Primeiro></Primeiro>
            </Card>
        </div>
    </div>
