export default [
    {id: 1, nome: 'Ana', nota: 4.2},
    {id: 2, nome: 'Lucas', nota: 8},
    {id: 3, nome: 'Pedro', nota: 9.2},
    {id: 4, nome: 'Gui', nota: 10},
    {id: 5, nome: 'Carlos', nota: 5.9},
    {id: 6, nome: 'Antonio', nota: 3},
]